package cl.android.appguia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import cl.android.appguia.network.RestClient;
import cl.android.appguia.network.request.Usuarios;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private TextView idUsuario, nombre, apellido, usuario;
    private Button botonConsulta, botonSiguiente, botonBorrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        idUsuario = findViewById(R.id.idUsuario);
        nombre = findViewById(R.id.nombre);
        apellido = findViewById(R.id.apellido);
        usuario = findViewById(R.id.usuario);
        botonConsulta = findViewById(R.id.bConsultar);
        botonSiguiente = findViewById(R.id.bSiguiente);
        botonBorrar = findViewById(R.id.bDelete);

        botonConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("El ID ES = "+idUsuario.getText().toString());
                RestClient.postSetup().getUsuario(idUsuario.getText().toString()).enqueue(new Callback<Usuarios>() {
                    @Override
                    public void onResponse(Call<Usuarios> call, Response<Usuarios> response) {
                        idUsuario.setText(response.body().getId());
                        nombre.setText(response.body().getNombre());
                        apellido.setText(response.body().getApellido());
                        usuario.setText(response.body().getNombre_usuario());
                        Toast.makeText(MainActivity.this, "Servicio consumido exitosamente", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Usuarios> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Falla en servicio", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        botonBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,BorrarActivity.class);
                startActivity(intent);
                /* System.out.println("Borra: El ID ES = "+idUsuario.getText().toString());
                RestClient.postSetup().delUsuario(idUsuario.getText().toString()).enqueue(new Callback<Usuarios>() {
                    @Override
                    public void onResponse(Call<Usuarios> call, Response<Usuarios> response) {

                    }

                    @Override
                    public void onFailure(Call<Usuarios> call, Throwable t) {

                    }
                }); */
            }
        });

        botonSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* System.out.println("Siguiente actividad = "+idUsuario.getText().toString()); */
                Intent intent = new Intent(MainActivity.this,EditarActivity.class);
                startActivity(intent);

            }
        });
    }
}
