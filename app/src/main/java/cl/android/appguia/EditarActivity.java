package cl.android.appguia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import cl.android.appguia.network.RestClient;
import cl.android.appguia.network.request.Usuarios;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarActivity extends AppCompatActivity {
    private TextView idUsuario, nombre, apellido, usuario;
    private Button botonGrabar, botonConsulta, botonBack;
    private Usuarios post = new Usuarios();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);

        this.idUsuario = findViewById(R.id.idUser);
        this.nombre = findViewById(R.id.tnombre);
        this.apellido = findViewById(R.id.tapellido);
        this.usuario = findViewById(R.id.tusuario);
        this.botonGrabar = findViewById(R.id.bGrabar);
        this.botonBack = findViewById(R.id.bBack);
        this.botonConsulta = findViewById(R.id.bQuery);

        botonConsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("El ID ES = "+idUsuario.getText().toString());
                /* RestClient.postSetup().updateUsuario(idUsuario.getText().toString()).enqueue(); */
                RestClient.postSetup().getUsuario(idUsuario.getText().toString()).enqueue(new Callback<Usuarios>() {
                    @Override
                    public void onResponse(Call<Usuarios> call, Response<Usuarios> response) {
                        /* idUsuario.setText(response.body().getId()); */

                        nombre.setText(response.body().getNombre());
                        apellido.setText(response.body().getApellido());
                        usuario.setText(response.body().getNombre_usuario());

                        post.setId(idUsuario.toString());
                        post.setNombre(response.body().getNombre());
                        post.setApellido(response.body().getApellido());
                        post.setNombre_usuario(response.body().getNombre_usuario());

                        System.out.println("idUser = "+post.getId());
                        System.out.println("Nombre = "+post.getNombre());
                        System.out.println("Apellido = "+post.getApellido());
                        System.out.println("User name = "+post.getNombre_usuario());

                        Toast.makeText(EditarActivity.this,"Obtenido Correctamete!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Usuarios> call, Throwable t) {
                        Toast.makeText(EditarActivity.this, "Error en sl conultar", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        botonGrabar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                /* System.out.println("El ID ES = "+idUsuario.getText().toString()); */
                /* RestClient.postSetup().updateUsuario(idUsuario.getText().toString()).enqueue(); */

                /* nombre = findViewById(R.id.tnombre);
                apellido = findViewById(R.id.tapellido);
                usuario = findViewById(R.id.tusuario); */

                post.setId(findViewById(R.id.idUser).toString());
                post.setNombre(nombre.getText().toString());
                post.setApellido(apellido.getText().toString());
                post.setNombre_usuario(usuario.getText().toString());

                System.out.println("idUser = "+post.getId());
                System.out.println("Nombre = "+post.getNombre());
                System.out.println("Apellido = "+post.getApellido());
                System.out.println("Usuario = "+post.getNombre_usuario());

                RestClient.postSetup().editUsuario(idUsuario.getText().toString(), post).enqueue(new Callback<Usuarios>() {


                    @Override
                    public void onResponse(Call<Usuarios> call, Response<Usuarios> response) {
                        /* idUsuario.setText(response.body().getId()); */
                        /* nombre.setText(response.body().getNombre());
                        apellido.setText(response.body().getApellido());
                        usuario.setText(response.body().getNombre_usuario()); */

                        //Toast.makeText(EditarActivity.this,"Editado Correctamete!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Usuarios> call, Throwable t) {
                        //Toast.makeText(EditarActivity.this, "Error en Edicion", Toast.LENGTH_SHORT).show();
                    }


                });
            }
        });
        botonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditarActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
